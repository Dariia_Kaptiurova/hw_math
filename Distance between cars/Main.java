package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите скорость первого автомобиля в км/час: ");

        double v1 = scanner.nextDouble();

        System.out.print("Введите скорость второго автомобиля в км/час: ");

        double v2 = scanner.nextDouble();

        System.out.print("Введите начальное расстояние между автомобилями в км: ");

        double s = scanner.nextDouble();

        System.out.print("Введите время движения в часах: ");

        double t = scanner.nextDouble();

        double distance = (v1 + v2) * t + s;

        System.out.println("Расстояние между автомобилями после поездки: " + distance + "км");
    }
}
