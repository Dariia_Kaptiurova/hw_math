package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите начальную скорость снаряда в км/час: ");

        double v = scanner.nextDouble() * 3.6;

        System.out.print("Введите угол возвышения ствола: ");

        double a = scanner.nextDouble();

        System.out.println("Выберите единицы измерения угла возвышения ствола: 1 - градусы;" +
                "\n" + "                                                   2 - рады");

        int g = scanner.nextInt();

        switch (g) {
            case (1):
                double projectileRange = (Math.pow(v, 2) * Math.sin(2 * a)) / 9.81;
                System.out.println("Расстояние полета снаряда: " + projectileRange);
                break;
            case (2):
                double projectileRange2 = (Math.pow(v, 2) * Math.sin(2 * Math.toDegrees(a))) / 9.81;
                System.out.println("Расстояние полета снаряда: " + projectileRange2);
                break;
            default:
                System.out.println("Неверный формат угла");
        }
    }
}
