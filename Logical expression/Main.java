package com.company;

public class Main {

    public static void main(String[] args) {

        double x = 0;
        double y = 0;

        int pointInArea = point(x, y);

        System.out.println(pointInArea);
    }

    public static int point(double x, double y) {

        int point = 0;

        if ((x >= 0) && (y >= 1.5 * x - 1) && (y <= x) || (x <= 0) && (y >= 1.5 * x - 1) && (y <= -x)) {
            point = 1;
        }
        return point;
    }
}